# "ported" by Adam Miller <maxamillion@fedoraproject.org> from
#   https://github.com/fedora-cloud/Fedora-Dockerfiles
#
# Originally written for Fedora-Dockerfiles by
#   scollier <emailscottcollier@gmail.com>

FROM centos:centos6
MAINTAINER The CentOS Project <cloud-ops@centos.org>

# Install the appropriate software
RUN yum -y update
RUN yum -y install epel-release
RUN yum -y install x11vnc xorg-x11-fonts* xorg-x11-xinit xorg-x11-xinit-session xorg-x11-server-Xvfb tigervnc-server xterm xorg-x11-font dejavu-sans-fonts dejavu-serif-fonts xdotool icewm* seamonkey openssh-server openssh-clients ; yum -y clean all

# Add the xstartup file into the image
ADD ./xstartup /
ADD ./jdk-6u45-linux-amd64.rpm /
ADD ./userscript.sh /

RUN yum -y install /jdk-6u45-linux-amd64.rpm;  rm -f /jdk-6u45-linux-amd64.rpm ;yum -y clean all
RUN mkdir ~/.vnc
RUN mv -f ./xstartup ~/.vnc/.
RUN chmod -v +x ~/.vnc/xstartup
RUN ln -s /usr/java/latest/jre/lib/amd64/libnpjp2.so /usr/lib64/seamonkey/plugins/
#RUN x11vnc -storepasswd 123456 ~/.vnc/passwd
ADD ./startilo.sh /
RUN mkdir -p /root/.mozilla/seamonkey/i61nnqcd.default/
ADD ./profiles.ini /root/.mozilla/seamonkey
ADD ./mimeTypes.rdf /root/.mozilla/seamonkey/i61nnqcd.default/


EXPOSE 5901
EXPOSE 22

CMD    ["/userscript.sh"]
#CMD    ["vncserver", "-fg","-geometry","1280x1024" ]
# ENTRYPOINT ["vncserver", "-fg" ]

#!/bin/bash
if [ "${iLO_USER}" != "" ]
then
    SESSION_KEY=""
    data="{\"method\":\"login\",\"user_login\":\"${iLO_USER}\",\"password\":\"${iLO_PASS}\"}"
    SESSION_KEY=$(curl -k -X POST "${iLO_HOST}/json/login_session" -d "$data" 2>/dev/null | grep -Eo '"session_key":"[^"]+' | sed 's/"session_key":"//')
/usr/bin/seamonkey "https://${iLO_HOST}/html/java_irc.html?sessionKey=${SESSION_KEY}" &
elif [ "${iLO_HOST}" != "" ]
then
    /usr/bin/seamonkey "https://${iLO_HOST}" &
else
    /usr/bin/seamonkey &
fi
